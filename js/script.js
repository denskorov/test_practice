const grid = document.querySelector('.grid');
const modal = $('#modal')

const msnry = new Masonry(grid, {
    // options...
    itemSelector: '.grid-item',
    columnWidth: '.grid-item',
    // gutter: 10
});

const ajaxConfig = {
    params: {
        _limit: 10
    }
}
axios.get('https://jsonplaceholder.typicode.com/photos', ajaxConfig)
    .then(({data: photos}) => {
        for (const photo of photos) {
            addGridElement(msnry, grid, photo)
        }

        msnry.layout()
    })

function addGridElement(grid, nodeGrid, obj) {
    const gridItemHtml = $(`<div class="grid-item"><img src="${obj.url}" alt=""></div>`)
    const elem = gridItemHtml[0]//document.createElement(gridItemHtml)

    nodeGrid.appendChild(elem);
    grid.appended(elem);
    grid.layout();

    addClickHandler(grid, elem, obj)
}

let activeGridItem = null

function addClickHandler(grid, element, photo) {
    element.addEventListener('click', function () {
        this.classList.toggle('big')
        grid.layout()

        if (activeGridItem) {
            activeGridItem.classList.remove('active')
        }

        this.classList.add('active')
        activeGridItem = this

        modal.find('#modalTitle').text(photo.title)
        modal.find('#modalUrl').attr('src', photo.url)

        modal.modal('show')

        // gridItems.forEach((item) => {
        //     item.classList.remove('active')
        //     this.classList.add('active')
        // })

    })

}
